// components/App.js
export default function App() {
    return (
        <div className="container">
            <section className="section is-medium mt-6">
                <h1 className="title is-1 mb-6">
                    React-light with CDN 🚀
                </h1>
                <h2 className="subtitle is-3">
                    Happy coding 😁!
                </h2>
                <p>Now you have React & JSX support in the browser thanks to babel standalone library, enjoy 🥳.</p>
                <p><a href="https://blog.maxds.fr/react-light/" target="_blank">How it works</a>.</p>
            </section>
        </div>
    );
}