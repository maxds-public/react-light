react-light
===========

Minimal React project for prototyping.

It provides React & JSX support in the browser using babel standalone library. It's for fast prototyping or tooling, not for production websites 😉

Just run `python -m http.server` (if you have Python 3 installed) and navigate to `cdn` or `standalone` directories.

If you want a server with live-reload capability, use `npx live-server` instead.

Blog post related to this project: https://blog.maxds.fr/react-light/
